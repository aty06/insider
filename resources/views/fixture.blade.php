@extends('master')

@section('title', 'Fixture Overview')

@section('content')
    <div class="container mt-5">
        <h3 class="card-title m-b-0">Fixture Overview</h3>
        <div class="row">
            @foreach($fixture as $week)
            <div class="col-3 m-1">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title m-b-0">Week {{ $week['week'] }}</h5>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Matches</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($week['matches'] as $match)
                            <tr>
                                <td>{{ $match['home_name'] }} <strong>{{ $match['home_team_score'] }} - {{ $match['away_team_score'] }}</strong> {{ $match['away_name'] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @endforeach
        </div>
        @if($present_week == 0)
            <a type="button" href="/play-next-week" class="btn btn-info m-2">Start Simulation</a>
        @else
            <a type="button" href="/" class="btn btn-info m-2">Dashboard</a>
        @endif
    </div>
@endsection
