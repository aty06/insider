<?php

use Illuminate\Support\Facades\Route;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\TournamentController@application');
Route::get('/generate-fixtures', 'App\Http\Controllers\TournamentController@generateFixtures');
Route::get('/fixture', 'App\Http\Controllers\TournamentController@fixtureOverview');
Route::get('/play-next-week', 'App\Http\Controllers\TournamentController@playNextWeek');
Route::get('/play-all-weeks', 'App\Http\Controllers\TournamentController@playAllWeeks');
Route::get('/reset-league', 'App\Http\Controllers\TournamentController@resetTournament');

/*Route::get('/', function () {
    return view('welcome');
});*/
