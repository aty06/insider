<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFixtureTable extends Migration
{
    public function up()
    {
        Schema::create('fixture', function (Blueprint $table) {

		$table->bigIncrements('id');
		$table->integer('week');
		$table->bigInteger('home_team_id')->unsigned();
        $table->foreign('home_team_id')->references('id')->on('team');
		$table->bigInteger('away_team_id')->unsigned();;
        $table->foreign('away_team_id')->references('id')->on('team');
		$table->integer('home_team_score')->nullable();
		$table->integer('away_team_score')->nullable();
		$table->tinyInteger('played');

        });
    }

    public function down()
    {
        Schema::dropIfExists('fixture');
    }
}
