<?php

namespace App\Http\Controllers;

use App\Models\Team;
use App\Models\Tournament;

class TournamentController extends Controller
{
    public function application()
    {
        $tournament = new Tournament();

        if (!$tournament->isFixtureGeneratedBefore()) {

            $teams = Team::getTeamsList();

            return view('welcome', $teams);
        }

        $presentWeek = $tournament->getPresentWeek();
        $leagueTable = $tournament->getLeagueTable();
        $fixture = $tournament->getFixture();
        $isLeagueFinished = $tournament->isLeagueFinished();
        $predictions = $tournament->getPredictions($leagueTable);

        return view('dashboard', ['week' => $presentWeek, 'leagueTable' => $leagueTable, 'fixture' => $fixture, 'predictions' => $predictions, 'finished' => $isLeagueFinished]);
    }

    public function generateFixtures()
    {
        $tournament = new Tournament();

        $result = $tournament->generateFixtures();

        if ($result['success'] == false)
            return $result;

        return redirect('/fixture');
    }

    public function fixtureOverview()
    {
        $tournament = new Tournament();

        $fixture = $tournament->getAllFixture();

        return view('fixture', ['fixture' => $fixture, 'present_week' => $tournament->getPresentWeek()]);
    }

    public function playNextWeek()
    {
        $tournament = new Tournament();

        $tournament->playNextWeek();

        return redirect('/');
    }

    public function playAllWeeks()
    {
        $tournament = new Tournament();

        $tournament->playAllWeeks();

        return redirect('/');
    }

    public function resetTournament()
    {
        $tournament = new Tournament();

        $result = $tournament->resetTournament();

        if (!$result)
            return json_encode([
                'success' => false
            ]);

        return redirect('/');
    }
}
