@extends('master')

@section('title', 'Dashboard')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-6">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title m-b-0">League Table</h5>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Team Name</th>
                            <th scope="col">P</th>
                            <th scope="col">W</th>
                            <th scope="col">D</th>
                            <th scope="col">L</th>
                            <th scope="col">GD</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($leagueTable as $row)
                            <tr>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->points }}</td>
                                <td>{{ $row->won }}</td>
                                <td>{{ $row->drawn }}</td>
                                <td>{{ $row->lost }}</td>
                                <td>{{ $row->goal_difference }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title m-b-0">Week {{$week}}</h5>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Matches</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($fixture as $match)
                            <tr>
                                <td>{{ $match->home_name }} <strong>{{ $match->home_team_score }} - {{ $match->away_team_score }}</strong> {{ $match->away_name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-3">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title m-b-0">Championship Predictions</h5>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Team Name</th>
                            <th scope="col">%</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($predictions as $prediction)
                            <tr>
                                <td>{{ $prediction['team_name'] }}</td>
                                <td>{{ $prediction['prediction'] }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            @if (!$finished)
                <a type="button" href="/play-all-weeks" class="btn btn-info m-2">Play All Weeks</a>
                <a type="button" href="/play-next-week" class="btn btn-info m-2">Play Next Week</a>
            @endif
            <a type="button" href="/reset-league" class="btn btn-danger m-2">Reset Data</a>
            <a type="button" href="/fixture" class="btn btn-success m-2">Fixture Overview</a>
        </div>
    </div>
@endsection
