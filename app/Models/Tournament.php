<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;

class Tournament extends Model
{
    use HasFactory;

    private int $presentWeek;

    public function __construct()
    {
        $this->calculatePresentWeek();

        parent::__construct();
    }

    private function calculatePresentWeek()
    {
        $present = DB::table('fixture')
            ->select('week')
            ->where('played', 1)
            ->orderBy('week', 'desc')
            ->take(1)
            ->value('week');

        if (!$present)
            $this->presentWeek = 0;
        else
            $this->presentWeek = $present;
    }

    /**
     * @return int
     */
    public function getPresentWeek(): int
    {
        return $this->presentWeek;
    }

    public function generateFixtures(): array
    {
        if ($this->isFixtureGeneratedBefore())
            return [
                'success' => false,
                'message' => 'Fixture had been generated before!'
            ];

        $list = Team::getTeamsList();
        $teamsCount = $list['teams']->count();
        $totalWeeks = ($teamsCount * 2) - 2;
        $fixture = [];

        for ($i = 1; $i <= $totalWeeks; $i++) {
            $fixture['weeks'][$i] = [];
        }

        $teamIDs = [];

        foreach ($list['teams'] as $team) {
            $teamIDs[] = $team->id;
        }

        $matchesNeedToBePlayed = [];

        foreach ($list['teams'] as $team) {
            foreach ($teamIDs as $id) {
                if ($id == $team->id)
                    continue;
                $matchesNeedToBePlayed[$team->id][] = $id;
            }
        }

        foreach ($matchesNeedToBePlayed as $home => $away) {
            foreach ($away as $val) {
                $randomWeek = rand(1, $totalWeeks);

                while (!$this->checkIfTeamsAvaliableForTheWeek($fixture, $randomWeek, $home, $val))
                    $randomWeek = rand(1, $totalWeeks);

                $fixture['weeks'][$randomWeek][] = $home . ':' . $val;
            }
        }

        foreach ($fixture['weeks'] as $week => $matches) {
            foreach ($matches as $match) {
                [$home, $away] = explode(':', $match);

                DB::table('fixture')->insert(
                    [
                        'week' => $week,
                        'home_team_id' => $home,
                        'away_team_id' => $away,
                        'played' => 0
                    ]
                );
            }
        }

        $this->initializeLeagueTable($teamIDs);

        return [
            'success' => true,
            'message' => 'Fixture generated!'
        ];
    }

    public function isFixtureGeneratedBefore(): bool
    {
        $present = DB::table('fixture')
            ->select('id')
            ->take(1)
            ->get();

        if (!$present->count())
            return false;

        return true;
    }

    private function checkIfTeamsAvaliableForTheWeek(array $fixture, int $selectedWeek, int $homeID, int $awayID): bool
    {
        foreach ($fixture['weeks'][$selectedWeek] as $match) {
            if (str_contains($match, $homeID) || str_contains($match, $awayID))
                return false;
            //echo 'home: ' . $homeID . ' -- away: ' . $awayID . ' -- match: ' .$match;
        }

        return true;
    }

    private function initializeLeagueTable(array $teamIDs): bool
    {
        foreach ($teamIDs as $id)
            DB::table('league_table')->insert(
                [
                    'team_id' => $id,
                    'points' => 0,
                    'won' => 0,
                    'drawn' => 0,
                    'lost' => 0,
                    'goal_difference' => 0
                ]
            );

        return true;
    }

    public function getLeagueTable(): Collection
    {
        return DB::table('league_table')
            ->join('team', 'team.id', '=', 'league_table.team_id')
            ->orderBy('points', 'desc')
            ->get();
    }

    public function getAllFixture(): array
    {
        $fixtures = DB::table('fixture')
            ->select('home_team.name as home_name', 'away_team.name as away_name', 'fixture.*')
            ->join('team as home_team', 'home_team.id', '=', 'fixture.home_team_id')
            ->join('team as away_team', 'away_team.id', '=', 'fixture.away_team_id')
            ->orderBy('week')
            ->get();

        $weeks = [];

        foreach ($fixtures as $record) {
            $weeks[$record->week]['week'] = $record->week;
            $weeks[$record->week]['matches'][] = [
                'home_name' => $record->home_name,
                'away_name' => $record->away_name,
                'home_team_score' => $record->home_team_score,
                'away_team_score' => $record->away_team_score,
            ];
        }

        return $weeks;
    }

    public function getPredictions($leagueTable): array
    {
        $list = Team::getTeamsList();
        $predictions = [];

        foreach ($list['teams'] as $team) {
            $predictions[$team->id] = [
                'team_name' => $team->name,
                'team_power' => $team->power
            ];
        }

        $teamsToBeOptimized = count($predictions);

        foreach ($predictions as $key => $prediction)
            $predictions[$key]['prediction'] = 100 / $teamsToBeOptimized;


        $this->optimizePredictionsBasedOnTeamPower($predictions, $teamsToBeOptimized);
        $this->isTherePossibilityToBecomeChampion($predictions, $leagueTable, $teamsToBeOptimized);
        $this->optimizePredictionsBasedOnLeaguePoints($predictions, $leagueTable, $teamsToBeOptimized);
        $this->finalOptimization($predictions);

        //echo '<pre>';print_r($predictions);die;

        return $predictions;
    }

    private function optimizePredictionsBasedOnTeamPower(array &$predictions, int &$teamsToBeOptimized)
    {

        foreach ($predictions as $key => $prediction) {

            $optimizationValue = 0;

            if ($prediction['team_power'] > 75)
                $optimizationValue = 15;
            elseif ($prediction['team_power'] > 50)
                $optimizationValue = 10;
            elseif ($prediction['team_power'] > 25)
                $optimizationValue = 5;

            if ($optimizationValue != 0 && $prediction['prediction'] != 100) {
                $predictions[$key]['prediction'] += $optimizationValue;

                foreach ($predictions as $_key => $_prediction) {
                    if ($_key == $key || $_prediction['prediction'] == 0.0)
                        continue;

                    $predictions[$_key]['prediction'] -= $optimizationValue / ($teamsToBeOptimized - 1);
                }
            }

        }
    }

    private function isTherePossibilityToBecomeChampion(array &$predictions, $leagueTable, &$teamsToBeOptimized)
    {
        $totalWeeks = (count($predictions) * 2) - 2;
        $weeksLeft = $totalWeeks - $this->presentWeek;
        $maxPointsToBeGained = $weeksLeft * 3;
        $leaderPoints = $this->leagueLeaderPoints($leagueTable);

        foreach ($leagueTable as $row) {
            if ($row->points == $leaderPoints)
                continue;

            if ($maxPointsToBeGained + $row->points < $leaderPoints) {
                $initialPrediction = $predictions[$row->team_id]['prediction'];
                $predictions[$row->team_id]['prediction'] = 0.0;
                $teamsToBeOptimized--;

                foreach ($predictions as $_key => $_prediction) {
                    if ($_key == $row->team_id || $_prediction['prediction'] == 0.0)
                        continue;

                    $predictions[$_key]['prediction'] += $initialPrediction / ($teamsToBeOptimized);
                }
            }
        }
    }

    private function leagueLeaderPoints($leagueTable): int
    {
        $max = 0;
        foreach ($leagueTable as $row) {
            if ($row->points > $max)
                $max = $row->points;
        }

        return $max;
    }

    private function optimizePredictionsBasedOnLeaguePoints(array &$predictions, $leagueTable, int $teamsToBeOptimized)
    {
        $highestPoints = $this->leagueLeaderPoints($leagueTable);
        $secondHighestPoints = $this->leagueNextPoints($leagueTable, $highestPoints);
        $thirdHighestPoints = $this->leagueNextPoints($leagueTable, $secondHighestPoints);

        foreach ($leagueTable as $row) {
            if ($predictions[$row->team_id]['prediction'] == 0 || $predictions[$row->team_id]['prediction'] == 100)
                continue;

            $optimizationValue = 0;

            if ($row->points == $highestPoints)
                $optimizationValue = 25;
            elseif ($row->points == $secondHighestPoints)
                $optimizationValue = 10;
            elseif ($row->points == $thirdHighestPoints)
                $optimizationValue = 5;

            if ($optimizationValue != 0) {
                $predictions[$row->team_id]['prediction'] += $optimizationValue;

                foreach ($predictions as $_key => $_prediction) {
                    if ($_key == $row->team_id || $_prediction['prediction'] == 0.0)
                        continue;

                    $predictions[$_key]['prediction'] -= $optimizationValue / ($teamsToBeOptimized - 1);
                }
            }
        }
    }

    private function leagueNextPoints($leagueTable, int $pointsLowerThen): int
    {
        $max = 0;
        foreach ($leagueTable as $row) {
            if ($row->points > $max && $row->points < $pointsLowerThen)
                $max = $row->points;
        }

        return $max;
    }

    private function finalOptimization(array &$predictions)
    {
        $totalPrediction = 0;
        foreach ($predictions as $prediction)
            $totalPrediction += $prediction['prediction'];

        $correctionAmount = $totalPrediction / 100;

        foreach ($predictions as $key => $prediction)
            $predictions[$key]['prediction'] = $prediction['prediction'] / $correctionAmount;
    }

    public function playAllWeeks(): bool
    {
        while (!$this->isLeagueFinished())
            $this->playNextWeek();

        return true;
    }

    public function isLeagueFinished(): bool
    {
        $present = DB::table('fixture')
            ->select('id')
            ->where('played', 0)
            ->take(1)
            ->get();

        if (!$present->count())
            return true;

        return false;
    }

    public function playNextWeek(): bool
    {
        return $this->playWeek($this->presentWeek + 1);
    }

    private function playWeek(int $week): bool
    {
        if ($this->isLeagueFinished())
            return false;

        $fixture = $this->getFixture($week);

        foreach ($fixture as $match) {
            $scores = $this->playMatch(new Team($match->home_team_id), new Team($match->away_team_id));
            $this->updateFixture($match->id, $scores);
            $this->updateLeagueTable($match->home_team_id, $match->away_team_id, $scores);
        }

        $this->presentWeek++;

        return true;
    }

    public function getFixture(int $week = null): Collection
    {
        if (!$week)
            $week = $this->presentWeek == 0 ? 1 : $this->presentWeek;

        return DB::table('fixture')
            ->select('home_team.name as home_name', 'away_team.name as away_name', 'fixture.*')
            ->join('team as home_team', 'home_team.id', '=', 'fixture.home_team_id')
            ->join('team as away_team', 'away_team.id', '=', 'fixture.away_team_id')
            ->where('week', '=', $week)
            ->get();
    }

    private function playMatch(Team $home, Team $away): array
    {
        $homeScore = rand(0, $this->calculateGoalPossibility($home->power, true));
        $awayScore = rand(0, $this->calculateGoalPossibility($away->power));

        return [
            'home' => $homeScore,
            'away' => $awayScore
        ];
    }

    public function calculateGoalPossibility($power, $isHome = false): int
    {

        $additionalScore = $isHome ? 1 : 0;

        if ($power <= 20)
            return 1 + $additionalScore;
        elseif ($power <= 40)
            return 2 + $additionalScore;
        elseif ($power <= 60)
            return 3 + $additionalScore;
        elseif ($power <= 80)
            return 4 + $additionalScore;
        else
            return 5 + $additionalScore;
    }

    private function updateFixture(int $matchID, array $scores): bool
    {
        DB::table('fixture')
            ->where('id', $matchID)
            ->update([
                'home_team_score' => $scores['home'],
                'away_team_score' => $scores['away'],
                'played' => 1
            ]);

        return true;
    }

    private function updateLeagueTable(int $homeID, int $awayID, $scores): bool
    {
        if ($scores['home'] > $scores['away']) {
            $homePoints = 3;
            $awayPoints = 0;
            $homeWin = 1;
            $awayWin = 0;
            $homeDrawn = 0;
            $awayDrawn = 0;
            $homeLoss = 0;
            $awayLoss = 1;
            $homeGoalDiff = $scores['home'] - $scores['away'];
            $awayGoalDiff = -$homeGoalDiff;
        } elseif ($scores['home'] < $scores['away']) {
            $homePoints = 0;
            $awayPoints = 3;
            $homeWin = 0;
            $awayWin = 1;
            $homeDrawn = 0;
            $awayDrawn = 0;
            $homeLoss = 1;
            $awayLoss = 0;
            $awayGoalDiff = $scores['away'] - $scores['home'];
            $homeGoalDiff = -$awayGoalDiff;
        } else {
            $homeWin = $awayWin = $homeLoss = $awayLoss = $homeGoalDiff = $awayGoalDiff = 0;
            $homePoints = $awayPoints = $homeDrawn = $awayDrawn = 1;
        }

        DB::table('league_table')
            ->where('team_id', $homeID)
            ->update([
                'points' => \DB::raw('points + ' . $homePoints),
                'won' => \DB::raw('won + ' . $homeWin),
                'drawn' => \DB::raw('drawn + ' . $homeDrawn),
                'lost' => \DB::raw('lost + ' . $homeLoss),
                'goal_difference' => \DB::raw('goal_difference + ' . $homeGoalDiff)
            ]);

        DB::table('league_table')
            ->where('team_id', $awayID)
            ->update([
                'points' => \DB::raw('points + ' . $awayPoints),
                'won' => \DB::raw('won + ' . $awayWin),
                'drawn' => \DB::raw('drawn + ' . $awayDrawn),
                'lost' => \DB::raw('lost + ' . $awayLoss),
                'goal_difference' => \DB::raw('goal_difference + ' . $awayGoalDiff)
            ]);

        return true;
    }

    public function resetTournament(): bool
    {
        DB::table('fixture')->truncate();
        DB::table('league_table')->truncate();

        return true;
    }

}
