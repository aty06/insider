<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeagueTableTable extends Migration
{
    public function up()
    {
        Schema::create('league_table', function (Blueprint $table) {

		$table->bigIncrements('id');
		$table->bigInteger('team_id')->unsigned();;
        $table->foreign('team_id')->references('id')->on('team');
		$table->integer('points');
		$table->integer('won');
		$table->integer('drawn');
		$table->integer('lost');
		$table->integer('goal_difference');

        });
    }

    public function down()
    {
        Schema::dropIfExists('league_table');
    }
}
