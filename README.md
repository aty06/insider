# INSIDER - Demo Application

This project is developed for implementing a demo application for Insider.

Project contains league fixture generation, league simulation and champion prediction.
Database migrations and seeder for the required simplest teams data included in the project.

##Prerequisites

* Git
* PHP 8.0
* Composer

##Installation

The project should be cloned from the repo to the desired location with the command below.

`git clone https://bitbucket.org/aty06/insider.git`

Framework dependencies must be installed

`composer install`

After the .env file created for your local environment database migrations must be executed

`php artisan migrate`

After migrations db seeder can be executed to insert teams data or manually `team` table must be filled 

`php artisan db:seed`

At this step applications is ready to use it can be used with any server or built in php server by following command

`php artisan serve`