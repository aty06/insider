<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Team extends Model
{
    use HasFactory;

    private int $id;
    private string $name;
    private int $power;

    /**
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;

        $team = DB::table('team')->select('id', 'name', 'power')->where('id', '=', $id)->orderBy('name')->get()->toArray();

        $this->name = $team[0]->name;
        $this->power = $team[0]->power;

        parent::__construct();
    }


    public static function getTeamsList(): array
    {
        $teams = DB::table('team')->select('id', 'name', 'power')->orderBy('name')->get();

        return ['teams' => $teams];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @param int $power
     */
    public function setPower(int $power): void
    {
        $this->power = $power;
    }
}
