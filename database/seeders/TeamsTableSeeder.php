<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('team')->insert(
            [
                'name' => 'Liverpool',
                'power' => 70
            ]
        );

        DB::table('team')->insert(
            [
                'name' => 'Chelsea',
                'power' => 80
            ]
        );

        DB::table('team')->insert(
            [
                'name' => 'Manchester',
                'power' => 90
            ]
        );

        DB::table('team')->insert(
            [
                'name' => 'Arsenal',
                'power' => 100
            ]
        );

    }
}
