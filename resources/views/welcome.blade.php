@extends('master')

@section('title', 'Tournament Teams')

@section('content')
    <div class="container mt-5">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body text-center">
                        <h5 class="card-title m-b-0">Tournament Teams</h5>
                    </div>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Team Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($teams as $team)
                            <tr>
                                <td>{{ $team->name }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
                <a type="button" href="/generate-fixtures" class="btn btn-info mt-2">Generate Fixtures</a>
            </div>
        </div>
    </div>
@endsection
