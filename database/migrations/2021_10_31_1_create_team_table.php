<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeamTable extends Migration
{
    public function up()
    {
        Schema::create('team', function (Blueprint $table) {

		$table->bigIncrements('id');
		$table->string('name',50);
		$table->integer('power');

        });
    }

    public function down()
    {
        Schema::dropIfExists('team');
    }
}
